A theme for petal

Usage
----------------------------------------

`Gemfile`:

    gem 'theme_gem_name', path: 'engines/theme_gem_name'

`app/assets/stylesheets/application.scss`:

    @import "theme/style";

`app/controllers/application_controller.rb`:

    class ApplicationController < ActionController::Base
      layout 'theme/application'
    end

