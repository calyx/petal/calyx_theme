module Theme
  module ThemeHelper
    protected

    def theme
      @theme ||= Theme::Config.new
    end

    def theme_sidebar_link(active:, path:, label:, icon:)
      active = active.is_a?(Hash) ? self.active(active) : active
      link_to(path, class: "nav-link #{active}") do
        icon(icon, content_tag(:span, label).html_safe)
      end
    end

    def theme_sidebar_section(label:, icon:)
      content_tag(:div, class: 'section') {
        icon(icon, content_tag(:span, label))
      } + content_tag(:div, '', class: 'section-rule')
    end
  end
end