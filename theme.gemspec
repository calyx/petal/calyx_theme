Gem::Specification.new do |spec|
  spec.name        = "theme"
  spec.version     = "0.1.0"
  spec.authors     = ["elijah"]
  spec.email       = ["elijah@riseup.net"]
  spec.homepage    = "https://calyxinstitute.org"
  spec.summary     = "calyx theme"
  spec.description = "calyx theme"
  spec.license     = "MIT"
  
  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = ""

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://0xacab.org/calyx/petal/calyx_theme"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]
  end
end
